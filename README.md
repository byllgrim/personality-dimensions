## About

Trying to create a good system for personality metrics.


## Resources


### APA Examples

* https://www.apa.org/pubs/journals/features/xlm-0000097.pdf
* https://www.apa.org/pubs/journals/features/xlm-0000064.pdf
* https://www.apa.org/pubs/journals/features/xlm-0000137.pdf


### Structure of a Research Paper

* https://en.wikipedia.org/wiki/Academic_writing#Format
* https://en.wikipedia.org/wiki/Scientific_writing
* https://en.wikipedia.org/wiki/IMRAD
* https://en.wikipedia.org/wiki/Scientific_literature#Structure_and_style
