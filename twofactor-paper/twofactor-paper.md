---
title: An Axiomatic Theoretical Framework for a Two-factor Model of Personality
author: my name
date: date
...


---
classoption:
- twocolumn
...


---
keywords: [TODO_1, TODO_2]
...


---
abstract: |
  This paper tries to follow a systemized/structured method
  for deriving an axiom-based two-factor model of personality.
  First a method is developed for generating a tree structure
  to represent the aspects/elements/tags of the personality dimensions,
  using a vector/matrix for building axioms and extending the graph.
  Then a selection and filtering system is designed to find
  a balanced binary sub-tree within that graph.
  Then the two-factor system is derived from this binary tree.
  Additional sub-factors are explored.
  Finally, comparisons and parallels are made towards existing
  systems for personality metrics.
  Potential future developments are discussed.
  one-to-four-paragraph summary
...


## Introduction

"Motivation" (what is the topic, what is wrong with existing solutions).
"Existing research".

Psychometrics is useful for a whole list of reasons that need not be reiterated (predicting psychopathology, workplace role, social understanding, etc).
(personality in general, and not just specifically e.g. depression, intelligence, etc.)
(Personality/Temperament/Humors.)
(It has been discussed since ancient times.)
Models exist in different levels of detail and accuracy (big 5, ...).
There is value in the low-res models.
We want the best low-res model (highest validity, appropriate level of abstraction, irreducably complex, etc).
(The most popular scientific model is big 5, but it is too complex to be casually applicable.)
(Then there are less complex ones, but they are not "scientifically good enough".)
(Of the existing models, there are therefore a gap where a trade-off between complexity/usability and predicton/comprehensiveness does not exist yet.)
(Or, there exists many attempts at those, but they are not irreducably complex.)
There is no "commonfolk-friendly" model to use in everyday life, and scientific research is not the only area of life that could benefit from such models.

There is utility in psychometrics TODO.
There has been various attempts (Big 5 etc) TODO.
Many two-factor models have been proposed TODO.
The benefits of a simple lo res two-factor model (benefits of "dimensionality reduction" etc) TODO.
Starting points of models are arbitrary (see how many models exists, lacking rationale for their axioms) TODO.

Personality exists, we want to measure it, but what is it?
Measuring personality is interesting and useful (why? Intelligence? Cooperation? Just out of curiousity?).
Each personality is very special, but a low res view can be used to simplify evaluations of people.
Many attempts have been made, but none can be said to be very successfully in gaining approvement.
The goal of this endeavor is to find a very simple system of measuring personality.
It aims to be very simple and reductionistic on purpose in order to be practicable to reason about.
It purposfully does a trade-off between reducing accuracy and the simplicity of reasoning about people.
Earlier systems have been based of text analysis or purely subjective conjecturing.
This system tries to make a more fundamental systematic axiomatic approach.
It does this by starting with the smallest primitives imaginable and building up from first principles.
The validity (what?) must be high, and the axes/dimensions must be orthogonal.
But the primary goal is to find the most minimal system, not the system most truly reflecting the reality of human personality.
It is likely that the resulting system will be very abstract and will thusly need a high degree of interpretation to map to real life observable behavior.
This study will also attempt to map the new system against existing systems to look for relations and possible interpretations.
Techniques from mathematics and logic will be employed, to ensure the method follow tried and true techniques.

The goal is to develop a tool that has the maximum cost of application, within a very tight budget.
Meaning that it should be the maximally useful tool that is useable within a low threshold of applicatory cost.
The low cost, i.e. simplicity of usage, increases the applicability (how?).
The "field" (or "space") spanned by the axes, should cover as broad an area of the human psyche as possible.
So the "ingredients" of the soup, before boiling it down, must include every possible ingredient that exists.
But instead of trying to find what those ingredients are, this method starts at the opposite end, starting with nothing and building up until everything.
A multi-dimensional system starts become too complex for applying assessments with speed.
While a one-dimensional system is so simple that it leaves room for unused capacity.
(It is meant to have a low cognitive load, so it is applicable in everyday casual scenarios and not just for research.)
(It might even be meant to be assessible casually by observation, without any survey or inventory.)

The criteria is 1) it must be BELOW a certain complexity level, and 2) within that bound it must have MAXIMUM explanatory and predictive power.
(what complexity level? how to measure?)
(explanatory/predictive power means to cover everything the big 5 does, etc. Albeit with low accuracy. The "widest spanning factor analysis".)
(Explanatory power means that "N/A" should not be a relevant answer for anyone.)
So the question is, what should the dimensions be?
("What is the number 1 trait that separates all humans?")
("What is the number 2 trait that separates all humans?")
(Or, what is the top two pair of trait that most broadly partitions all humans?)
("two degrees of variability".)
"Dimensionality reduction", "factor analysis", "Operationalization", "curse of dimensionality", ?.

background for the research including a discussion of similar research.

Primary thesis: A model can be SYNTHESIZED to give the broadest possible expressive power, and can be classified/contain to levels of complexity.
Secondary thesis: Existing models will exist somewhere in the spaces spanned by these synthesized models.
Ternary thesis: The abstract models will "nominate a winner" among the existing models.


## Methods

Details of how the research was conducted.

The main goal is to produce the "abstract model".
A secondary goal is to create a framework for interpreting it, finding an appropriate "operationalization".


### Abstract Model

Building the model.
What exists?
A simple boolean will not have expressive power as it skips "degrees".
(possibility for "two views", one fine grain view and a simplified one?)
Synthesize a model.
The first piece of information introduced must be unanimously the only new piece of info that can be introduced.
If there is a choice of what to add, a "branch" must be made to follow each "thread" of possible models.

The complexity will start with 0 dimensions and increment upwards.
For each N number of dimensions in a model, the highest "prime model" iteration shall be picked.
(Or should the highest independence be used to pick?)
For level N to be recognized, adding a "component of complexity" to get level N+1 would have to raise the dimensionality of the system?
(E.g. {exist, not-exist} -> {exist "1", exist "0", not-exist} doesn't work, because not-exist can't be divided like exist can.)

Show that the existing systems are not good enough.
(They cannot all be true. They differ too much.)
(Big 5 is also based on lexical analysis, and that is almost completely arbitrary.)
Show that it is too difficult to work "backwards" from these unto the smallest irreducably complex common denominator subset.
it is not known what the "best" two-factor model is.

Method will be to expand from nothing, until the first thing that fits all criteria arises.
(This is then, not an ANALYSIS as previous models seem to have been derived from, but an experimental SYNTHESIS, the utility of which remains to be seen.)

Single measure on each dimension, or a "span"?
If the model shall be simple, then a single point, not a span.
But why add complexity via dimensions, instead of complexity via points on a single (or a few more) dimension(s)?
Could each point be regarded as its own dimension, as in being a "variable".


### Operationalization

Analytical terms "fit", "invariance", "validity", "correlation", ...
(See: Multigroup CFA, Mann Whitney W test, Spearman's correlation, coefficient alpha.)
other terms "factor structure", "covariance", "latent variable", "internal validity", ?

2-factor model. Can be easily illustrated, easily partitioned. (2x2 or 3x3?)

most fundamental piece of "existance". E.g. "ifdef"? "boolean"?

Mapping the abstract model's "fuzzy concept" to real life behavior or thoughts ("observable variables").
(Must it be directly observable behavior?)


## Results

TODO

Does it map to existing "two-factor models", hence "confirming" them?
Does it reveal any flaw in existing two-factor models?


## Discussion / Conclusion

limitations, implications, ?


## References

TODO

"DASS-8"

TODO: KNOWING SOFTWARE ENGINEER’S PERSONALITY TO IMPROVE SOFTWARE DEVELOPMENT

Psychometric Society, Psychometrika
