# personality-dimensions

proposals for mapping personality



## adjectives

gregarious <-> introvert(?)

practicality/dutiful <-> ?

maturity

logic, excitement, principles

concrete <-> meta

nominal <-> causal



## pre-existing

### mbti

ie, ns, tf, pj


### big 5

openness, conscientiousness, extraversion, agreeableness, neuroticism



## miscellaneous


### motivation

normal set:

    autonomy
    mastery
    purpose

adhd set:

    interest
    challenge
    novelty
    urgency


### user interfaces

some people prefer `orthogonality`, other prefer `convenience`.
the first is a "hacker" type, likes to create scripts instead of using gui.
the latter is a "shit ass windows user" who likes designer-mandated automation.


### latent inhibition

is there a connection between "latent inhibition" and "intuition"?



## analysis

can pre-existing systems have simple common denominators?
e.g. extraversion and agreeableness both have "more/less people".

the goal is to find one basis to rule them all.
and to not rely on adjectives, but _basic_ terms like "many/few" and "move towards/away".
all pre-existing dimensions would be combinations of these new ones.

definition of done:
found a small (~5) set that can reasonably (?) describe pre-existing dimensions.


### experimental attempt

guesses:
* more/less people
* more/less danger
* is sensitive to
* likes vs does
* known/unknown
* persistence (persistence is really about _movement_
* self/other
* loud/quie
* attention
* variation

basic terms:
* small/large
* one/many
* mental/physical
* close/far
* people/things
* take/give
* moving/still
* hard/soft
* plasticity/stiffness

categories of adjectives:
* colors
* time
* emotions
* size
* taste
* touch
* sound
* shape

math and physics:
* plus - grow/more/increase/big/unite
* minus - small/difference/change
* division - rate

axioms:
* exist
* size
* position
* direction
* time
* change
* combinations:
  * size + change = movement?
  * size = position?

https://en.wikipedia.org/wiki/list_of_materials_properties
https://www.electrical4u.com/characteristics-of-sensors/


### pre-existing

big5:
* o - more adventure, newer stuff/experiences
* c - more work, more action, more plan
* e - seek more contact with people
* a - less danger/hostility, with more people
* n - more defence!

disc:
* dominance - 
* inducement
* submission
* compliance

mbti:
* ei - 
* sn - narrow minded, small mind
* ft - 
* jp - 

keirsey:
* concrete/abstract
* cooperative/pragmatic
* informative/directive
* expressive/attentive

hexaco:
* ?


### conclusion(?)

modifiers+properties or adjectives+adjectives

* sensitive / unsusceptible
* plastic / rigid
* objects/emotions, things/people, rational/emotional
* dreams / facts
* self / others



## unsorted misc

seek / generate stimulation

rational emotional

concrete

questioning

satisfied

expressive (to people or to things)?

possibilities/speculations vs facts

real vs imaginary

absolute relative

changing

fire

ambitious (take)

relaxed (not sensitive)

sensitive + others = vanity?

clean dirty

give take
move (to/from)
thing people/self
give = move thing from self to people
take = move thing from people to self

improvised vs planned/scripted/scheduled

